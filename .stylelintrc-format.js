const common = require('./.stylelintrc.js');
const { merge } = require('lodash');

const format = {
  extends: [
    ...common.extends,
    'stylelint-config-prettier',
    'stylelint-config-recess-order',
  ],
};

const config = merge(common, format);

module.exports = config;
