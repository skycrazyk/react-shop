module.exports = {
  plugins: [
    [
      'react-css-modules',
      {
        generateScopedName: '[local]_[hash:base64:3]',
        webpackHotModuleReloading: true,
      },
    ],
  ],
};
