import React, { Component } from "react";
import {  HashRouter, Route } from "react-router-dom";
import cn from 'classnames';
import Header from '@comp/Header';
import Home from '@comp/Home';
import Cart from '@/components/Cart';
import style from './style.css';

export default class App extends Component<{}, {}> {
  render() {
    return (
      <HashRouter>
        <Header />
        <div className={cn('container', style.content)}>
          <Route path="/" exact component={Home} />
          <Route path="/cart/" component={Cart} />
        </div>
      </HashRouter>
    )
  }
}
