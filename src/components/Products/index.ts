import { connect } from "react-redux";
import { AppState } from '@/store/';
import { loadCatalog } from '@/store/catalog/actions';
import Products from './Products';

const mapState = (state: AppState) => ({
  ...state.catalog
});

const mapActions = {
  loadCatalog,
}

export default connect(
  mapState,
  mapActions,
)(Products);
