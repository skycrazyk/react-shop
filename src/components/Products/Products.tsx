import React, { PureComponent } from 'react';
import style from './style.css';
import { CatalogState } from '@store/catalog/types';
import { loadCatalog } from '@store/catalog/actions';
import ProductsItem from '@comp/ProductsItem';

interface ProductsProps extends CatalogState {
  loadCatalog: typeof loadCatalog,
}

export default class Products extends PureComponent<ProductsProps> {
  componentDidMount() {
    const { loadCatalog } = this.props;
    loadCatalog();
  }

  render() {
    const { products } = this.props;

    return (
      <ul className={style.list}>
        {products.map(product => (<ProductsItem product={product} key={product.id}/>))}
      </ul>
    )
  }
}
