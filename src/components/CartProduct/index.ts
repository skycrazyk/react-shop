import { connect } from "react-redux";
import { cartDecrement, cartIncrement, cartSetCount } from '@store/cart/actions';
import CartProduct from './CartProduct';

const mapActions = {
  cartDecrement,
  cartIncrement,
  cartSetCount
}

export default connect(
  null,
  mapActions
)(CartProduct);
