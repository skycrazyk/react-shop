import React, { PureComponent, ChangeEvent, KeyboardEvent, createRef, RefObject } from 'react';
import cn from 'classnames';
import CartRow from '@comp/CartRow';
import { Product } from '@store/cart/types';

import {
  cartDecrement,
  cartIncrement,
  cartSetCount,
} from '@store/cart/actions';

import style from './style.css';

interface CartProductProps {
  product: Product;
  cartDecrement: typeof cartDecrement;
  cartIncrement: typeof cartIncrement;
  cartSetCount: typeof cartSetCount;
}

export default class CartProduct extends PureComponent<CartProductProps> {
  inputRef: RefObject<HTMLInputElement> = createRef();

  edit = (event: ChangeEvent<HTMLInputElement> | KeyboardEvent<HTMLInputElement>) => {
    const { product, cartSetCount } = this.props;
    const value = Number(event.currentTarget.value);
    const count = value > 0 ? value : 1;

    cartSetCount({
      ...product,
      count,
    });
  }

  onKeyUpEdit = (event: KeyboardEvent<HTMLInputElement>) => {
    const { edit } = this;

    if (event.key === 'Enter') {
      edit(event);
    }
  }

  remove = () => {
    const { product, cartSetCount } = this.props;

    cartSetCount({
      ...product,
      count: 0,
    });
  }

  componentDidUpdate(prevProps: CartProductProps) {
    const { product: { count: prevCount } } = prevProps;
    const { product: { count } } = this.props;
    const { inputRef } = this;

    const isCountNotEqual = prevCount !== count;
    const isInputValueWrong = inputRef.current && Number(inputRef.current.value) !== count;

    if ((isCountNotEqual || isInputValueWrong) && inputRef.current) {
      inputRef.current.value = count.toString();
    }
  }

  render() {
    const { product, cartDecrement, cartIncrement } = this.props;
    const { edit, onKeyUpEdit, remove, inputRef } = this;

    return (
      <CartRow
        mode="th"
        preview={<img src={product.img} className={style.img} />}
        name={<span>{product.name}</span>}
        price={<span>{product.price}</span>}
        count={
          <div className={style.actions}>
            <button
              onClick={() => cartDecrement(product)}
              className={cn(style.action, style.action_decrement)}
            >
              -
            </button>
            <input
              ref={inputRef}
              type="number"
              min="0"
              step="1"
              className={style.field}
              defaultValue={product.count.toString()}
              onBlur={edit}
              onKeyUp={onKeyUpEdit}
            />
            <button
              onClick={() => cartIncrement(product)}
              className={style.action}
            >
              +
            </button>
          </div>
        }
        action={
          <button onClick={remove} className={style.remove}>×</button>
        }
      />
    );
  }
}
