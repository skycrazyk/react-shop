import React from 'react';
import style from './style.css';
import { Product } from '@store/catalog/types';
import { cartIncrement } from '@store/cart/actions';

interface ProductProps {
  product: Product,
  cartIncrement: typeof cartIncrement
}

export default function Products(props: ProductProps) {
  const { product, cartIncrement } = props;

  return (
    <li className={style.item}>
      <div className={style.inner}>
        <img className={style.img} src={product.img} alt={product.name} />
      </div>
      <div className={style.info}>
        <div>{product.name}</div>
        <div className={style.price}>{product.price}</div>
      </div>
      <div className={style.action}>
        <button className={style.btn} onClick={() => cartIncrement(product)}>
          Add to card
        </button>
      </div>
    </li>
  )
}
