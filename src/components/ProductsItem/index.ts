import { connect } from "react-redux";
import { cartIncrement } from '@/store/cart/actions';
import ProductsItem from './ProductsItem';

const mapActions = {
  cartIncrement,
}

export default connect(
  null,
  mapActions,
)(ProductsItem);
