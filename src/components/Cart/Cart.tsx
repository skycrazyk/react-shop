import React from 'react';
import { Cart } from '@store/cart/types';
import { cartCleanAll } from '@store/cart/actions';
import CartRow from '@comp/CartRow';
import CartProduct from '@comp/CartProduct'
import style from './style.css';

interface CartProps {
  cart: Cart;
  cartCleanAll: typeof cartCleanAll;
  totalPrice: number;
}

export default function Cart(props: CartProps) {
  const { cart, cartCleanAll, totalPrice } = props;

  return cart.length ? (
    <div>
      <table className={style.cart}>
        <tbody>
          <CartRow
            mode="th"
            preview={<span>Preview</span>}
            name={<span>Name</span>}
            price={<span>Price</span>}
            count={<span>Count</span>}
            action={<button onClick={cartCleanAll}>Delete all</button>}
          />
          {cart.map(product => (
            <CartProduct product={product} key={product.id} />
          ))}
        </tbody>
      </table>
      <div className={style.total}>Total: {totalPrice}</div>
    </div>
  ) : (
    <div className={style.empty}>The cart is empty</div>
  );
}
