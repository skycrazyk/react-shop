import { connect } from "react-redux";
import { AppState } from '@/store';
import { cartCleanAll } from '@store/cart/actions';
import totalPrice from '@/selectors/totalPrice';
import Cart from './Cart';

const mapState = (state: AppState) => ({
  cart: state.cart.cart,
  totalPrice: totalPrice(state)
})

const mapActions = {
  cartCleanAll
}

export default connect(
  mapState,
  mapActions
)(Cart);
