import React from "react";
import { Link } from "react-router-dom";
import cn from 'classnames';
import style from './style.css';

interface HeaderProps {
  countOfProducts: number;
}

export default function Header(props: HeaderProps) {
  const { countOfProducts } = props;

  return (
    <nav className={style.nav}>
      <div className={cn(style.inner, 'container')}>
        <div className={style.logo}>
          <span>Stickers</span>
        </div>
        <ul className={style.menu}>
          <li className={style.item}>
            <Link to="/">Home</Link>
          </li>
          <li className={style.item}>
            <Link to="/cart">Shopping cart ({countOfProducts})</Link>
          </li>
        </ul>
      </div>
    </nav>
  )
}
