import { connect } from "react-redux";
import Header from './Header';
import { AppState } from '@/store';
import countOfProducts from '@/selectors/countOfProducts';

const mapState = (state: AppState) => ({
  countOfProducts: countOfProducts(state),
})

export default connect(
  mapState,
)(Header);
