import React, { Component } from "react";
import Products from '@comp/Products';

type HomeProps = {
  // TODO: Router props
}

export default class Home extends Component<HomeProps> {
  render() {
    return (
      <>
        <div>
          <h1>Home</h1>
        </div>
        <Products />
      </>
    )
  }
}

