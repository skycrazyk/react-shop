import { createElement } from 'react';

interface CartRowProps {
  [key: string]: string | JSX.Element;
  mode: string;
  preview: JSX.Element;
  name: JSX.Element;
  price: JSX.Element;
  count: JSX.Element;
  action: JSX.Element;
}

export default function CartRow(props: CartRowProps) {
  const { mode } = props;
  const cols = ['preview', 'name', 'price', 'count', 'action'];

  return createElement('tr', null, ...cols.map(col => createElement(mode, null, props[col])))
}
