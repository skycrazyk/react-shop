import {
  CatalogState,
  CatalogActionTypes,
  CATALOG_REQUEST,
  CATALOG_SUCCESS,
  CATALOG_FAILURE,
} from "./types";

const initialState: CatalogState = {
  products: [],
  loading: false,
  error: undefined,
};

export function catalogReducer(
  state = initialState,
  action: CatalogActionTypes
): CatalogState {
  switch (action.type) {
    case CATALOG_REQUEST:
      return {
        ...state,
        loading: true,
        error: undefined,
      }

    case CATALOG_SUCCESS:
      return {
        ...state,
        products: action.payload,
        loading: false,
      };

    case CATALOG_FAILURE:
      return {
        ...state,
        error: action.payload,
        loading: false,
      };

    default:
      return state;
  }
}
