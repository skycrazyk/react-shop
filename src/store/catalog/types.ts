export interface Product {
  id: number;
  name: string;
  img: string;
  price: number;
}

export type Products = Product[];

export interface CatalogState {
  products: Products;
  loading: Boolean;
  error: Error | undefined; // TODO: Error must be redux-api-middleware Error object
}

export const CATALOG_REQUEST = "CATALOG_REQUEST";
export const CATALOG_SUCCESS = "CATALOG_SUCCESS";
export const CATALOG_FAILURE = "CATALOG_FAILURE";

interface RequestCatalogAction {
  type: typeof CATALOG_REQUEST;
}

interface SuccessCatalogAction {
  type: typeof CATALOG_SUCCESS;
  payload: Products;
}

interface FailureCatalogAction {
  type: typeof CATALOG_FAILURE;
  payload: Error,
}

export type CatalogActionTypes = RequestCatalogAction | SuccessCatalogAction | FailureCatalogAction;
