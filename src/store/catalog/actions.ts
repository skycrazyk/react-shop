import { CATALOG_REQUEST, CATALOG_SUCCESS, CATALOG_FAILURE } from "./types";
import { RSAA } from 'redux-api-middleware';

export const loadCatalog = () => ({
  [RSAA]: {
    endpoint: 'catalog.json',
    method: 'GET',
    types: [CATALOG_REQUEST, CATALOG_SUCCESS, CATALOG_FAILURE],
  }
})
