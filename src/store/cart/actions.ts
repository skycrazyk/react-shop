import {
  CART_INCREMENT,
  CART_DECREMENT,
  CART_SET_COUNT,
  CART_CLEAN_ALL,
  IncrementAction,
  DecrementAction,
  SetCountAction,
  CleanAllAction,
  Product
} from "./types";

import { Product as CatalogProduct } from '@store/catalog/types';

export function cartIncrement(product: Product | CatalogProduct): IncrementAction {
  return {
    type: CART_INCREMENT,
    payload: product,
  }
}

export function cartDecrement(product: Product): DecrementAction {
  return {
    type: CART_DECREMENT,
    payload: product,
  }
}

export function cartSetCount(product: Product): SetCountAction {
  return {
    type: CART_SET_COUNT,
    payload: product,
  }
}

export function cartCleanAll(): CleanAllAction {
  return {
    type: CART_CLEAN_ALL
  }
}
