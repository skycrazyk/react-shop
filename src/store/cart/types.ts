import { Product as CatalogProduct } from '@store/catalog/types';

export interface Product extends CatalogProduct {
  count: number;
}

// TODO заменить type на interface
export type Cart = Product[];

export interface CartState {
  cart: Cart;
}

export const CART_INCREMENT = "CART_INCREMENT";
export const CART_DECREMENT = "CART_DECREMENT";
export const CART_SET_COUNT = "CART_SET_COUNT";
export const CART_CLEAN_ALL = "CART_CLEAN_ALL";

export interface IncrementAction {
  type: typeof CART_INCREMENT;
  payload: Product | CatalogProduct;
}

export interface DecrementAction {
  type: typeof CART_DECREMENT;
  payload: Product;
}

export interface SetCountAction {
  type: typeof CART_SET_COUNT;
  payload: Product,
}

export interface CleanAllAction {
  type: typeof CART_CLEAN_ALL;
}

export type CartActionTypes = IncrementAction | DecrementAction | SetCountAction | CleanAllAction;
