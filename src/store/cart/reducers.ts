import { cloneDeep } from 'lodash';

import {
  CartState,
  CartActionTypes,
  CART_INCREMENT,
  CART_DECREMENT,
  CART_SET_COUNT,
  CART_CLEAN_ALL
} from "./types";


const initialState: CartState = {
  cart: [],
};

export function cartReducer(
  state = initialState,
  action: CartActionTypes
): CartState {
  const stateClone = cloneDeep(state);

  switch (action.type) {
    case CART_INCREMENT: {
      const same = stateClone.cart.find(item => item.id === action.payload.id);

      if (same) {
        same.count += 1;
      } else {
        stateClone.cart.unshift({
          ...action.payload,
          count: 1,
        });
      }

      return stateClone
    }

    case CART_DECREMENT: {
      const same = stateClone.cart.find(item => item.id === action.payload.id);

      if (same) {
        same.count -= 1;

        if (!same.count) {
          const index = stateClone.cart.indexOf(same);
          stateClone.cart.splice(index, 1);
        }
      }

      return stateClone
    }

    case CART_SET_COUNT:
      if (action.payload.count) {
        const same = stateClone.cart.find(item => item.id === action.payload.id);

        if (same) {
          same.count = action.payload.count;
        }
      } else {
        const index = stateClone.cart.findIndex(item => item.id === action.payload.id);
        stateClone.cart.splice(index, 1);
      }

      return stateClone

    case CART_CLEAN_ALL:
      stateClone.cart = [];
      return stateClone;

    default:
      return stateClone;
  }
}
