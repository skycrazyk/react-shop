import React from 'react';
import { render } from 'react-dom';
import { Provider } from "react-redux";
import configureStore from "@/store";
import '@/assets/global.css?global';
import App from '@comp/App';

// Hot module replacement
if (module.hot) {
  process.env.NODE_ENV === 'development' && module.hot.accept();
}

export type Settings = {
  selector: string;
}

const store = configureStore();

const Root = () => (
  <Provider store={store}>
    <App />
  </Provider>
);

export function create(settings: Settings) {
  render(<Root />, document.getElementById(settings.selector));
}
