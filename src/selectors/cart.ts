import { AppState } from '@/store/';

export default (state: AppState) => state.cart.cart;
