import { createSelector } from 'reselect';
import cart from './cart';

export default createSelector(
  cart,
  cart => cart.reduce((acc: number, current) => acc + current.count, 0)
);
