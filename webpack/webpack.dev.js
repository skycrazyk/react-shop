const merge = require('webpack-merge');
const { common, strategy } = require('./webpack.common.js');
const webpack = require('webpack');

module.exports = env =>
  merge.smartStrategy(strategy)(common(env), {
    mode: 'development',
    devtool: 'inline-source-map',
    devServer: {
      contentBase: './dist',
      hot: true,
    },
    module: {
      rules: [
        {
          test: /\.css$/,
          oneOf: [
            {
              resourceQuery: /global/,
              use: [
                {
                  loader: 'style-loader',
                  options: {
                    sourceMap: true,
                  },
                },
                {
                  loader: 'css-loader',
                  options: {
                    sourceMap: true,
                  },
                },
              ],
            },
            {
              use: [
                {
                  loader: 'style-loader',
                  options: {
                    sourceMap: true,
                  },
                },
                {
                  loader: 'css-loader',
                  options: {
                    sourceMap: true,
                    modules: {
                      localIdentName: '[local]_[hash:base64:3]',
                    },
                  },
                },
              ],
            },
          ],
        },
      ],
    },
    plugins: [new webpack.HotModuleReplacementPlugin()],
  });
