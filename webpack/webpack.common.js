const path = require('path');
const fs = require('fs');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const dotenv = require('dotenv');
const webpack = require('webpack');

const resolvePath = absPath => path.resolve(__dirname, '../', absPath);

/**
 * Возвращает объект с переменными окружения из соответствующего .env файла.
 * Переменные доступны с префиксом: REACT_ENV_название_переменной
 * @param {string} environment Название окружения, например "development". Необязательный параметр.
 */
function getEnvVariables(environment) {
  const defaultEnv = resolvePath('env/.env');
  const possibleEnv = `${defaultEnv}.${environment}`;
  const currentEnv = fs.existsSync(possibleEnv) ? possibleEnv : defaultEnv;

  const [defaultConfigEnv, currentConfigEnv] = [defaultEnv, currentEnv].map(
    configPath => {
      const resultConfigEnv = dotenv.config({
        path: configPath,
      }).parsed;

      return resultConfigEnv;
    }
  );

  const configEnv = Object.assign(defaultConfigEnv, currentConfigEnv); // Fallback to .env

  const stringifyEnv = Object.keys(defaultConfigEnv).reduce((acc, current) => {
    acc[`REACT_ENV_${current}`] = JSON.stringify(configEnv[current]);
    return acc;
  }, {});

  return { configEnv, stringifyEnv };
}

const common = (env = {}) => {
  const { ENVIRONMENT } = env;
  const { stringifyEnv } = getEnvVariables(ENVIRONMENT);

  return {
    entry: resolvePath('src/index.tsx'),
    output: {
      filename: '[name].min.js',
      path: resolvePath('dist'),
      library: 'reactshop',
    },
    module: {
      rules: [
        {
          test: /\.(png|svg|jpg|gif)$/,
          use: ['file-loader'],
        },
        {
          test: /\.tsx?$/,
          loader: 'ts-loader',
        },
        {
          // include node_modules/module/path in regular exp. if need
          include: /src/,
          enforce: 'pre',
          test: /\.(tsx?|js|css)$/,
          loader: 'source-map-loader',
        },
      ],
    },
    resolve: {
      extensions: ['.ts', '.tsx', '.js', '.json', '.css'],
      alias: {
        '@': resolvePath('src/'),
        '@comp': resolvePath('src/components/'),
        '@store': resolvePath('src/store/'),
        '@utils': resolvePath('src/utils/'),
      },
    },
    plugins: [
      new CleanWebpackPlugin(),
      new HtmlWebpackPlugin({
        title: 'React stickers shop',
        template: 'src/index.html',
        inject: 'head',
      }),
      new CopyWebpackPlugin([
        {
          from: 'public/**/*',
          // remove 'public/' from every path
          transformPath(targetPath) {
            return targetPath
              .split(path.sep)
              .slice(1)
              .join(path.sep);
          },
        },
      ]),
      new webpack.DefinePlugin(stringifyEnv),
    ],
  };
};

const strategy = {
  // 'module.rules.oneOf.use': 'prepend',
};

module.exports = {
  common,
  strategy,
};
