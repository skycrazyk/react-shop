const merge = require('webpack-merge');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const { common } = require('./webpack.common.js');

module.exports = env =>
  merge(common(env), {
    mode: 'production',
    module: {
      rules: [
        {
          test: /\.css$/,
          oneOf: [
            {
              resourceQuery: /global/,
              use: [
                {
                  loader: MiniCssExtractPlugin.loader,
                  options: {
                    sourceMap: true,
                  },
                },
                {
                  loader: 'css-loader',
                  options: {
                    sourceMap: true,
                  },
                },
              ],
            },
            {
              use: [
                {
                  loader: MiniCssExtractPlugin.loader,
                  options: {
                    sourceMap: true,
                  },
                },
                {
                  loader: 'css-loader',
                  options: {
                    sourceMap: true,
                    modules: {
                      localIdentName: '[local]_[hash:base64:3]',
                    },
                  },
                },
              ],
            },
          ],
        },
      ],
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: '[name].min.css',
      }),
    ],
    optimization: {
      minimizer: [
        new UglifyJsPlugin({
          cache: true,
          parallel: true,
          sourceMap: true,
        }),
        new OptimizeCSSAssetsPlugin(),
      ],
    },
  });
