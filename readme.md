# Виджет "React shop"

## Project setup

```
yarn
```

### Compiles and hot-reloads for development

```
yarn start
```

### Compiles and minifies for production

```
yarn build
```

## Usage

```
<head>
  ...
  <link href="main.min.css" rel="stylesheet">
  <script type="text/javascript" src="main.min.js"></script>
  ...
</head>
<body>
  ...
  <div id="app"></div>
  <script>
    new reactshop.create({
      selector: 'app',
    });
  </script>
  ...
</body>
```
